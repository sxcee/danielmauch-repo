#ifndef SCANNERIMPL_H_
#define SCANNERIMPL_H_
#include "Buffer.h"
#include "Scanner.h"
#include "IScanner.h"
#include "../../Symboltable/includes/Symboltable.h"
#include "../../Automat/includes/Automat.h"

class ScannerImpl: public Scanner, public IScanner {
public:
	ScannerImpl();
	~ScannerImpl();

	virtual void update(Buffer * buffer, Symboltable * symtab);

	virtual Token nextToken();
	virtual void freeToken(Token token);

	virtual void mkToken(TokenType type);
	virtual void ungetChar(int amount);
	virtual void stop();
private:
	Buffer * buffer;
	Symboltable * symtab;
	Automat * automat;
	int row;
	int column;
	TokenType tempType;
	char* lexem;
	int currentLength;
	bool makeToken;
	char lastChar;
	int tooMuchChars;
};

#endif
