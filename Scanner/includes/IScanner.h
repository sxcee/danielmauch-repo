
#ifndef ISCANNER_H_
#define ISCANNER_H_

#include <iostream>
using namespace std;

class IScanner {
public:
	virtual ~IScanner(){}

	virtual void mkToken(TokenType type) = 0;
	virtual void ungetChar(int amount) = 0;
	virtual void stop() = 0;
};

#endif
