
#ifndef BUFFER_H_
#define BUFFER_H_

class Buffer {
public:
	Buffer();
    ~Buffer();

	char getChar();
	void ungetChar(int n);
};
#endif
