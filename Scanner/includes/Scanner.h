/*
 * Scanner.h
 *
 *  Created on: Sep 26, 2012
 *      Author: knad0001
 */
#include "Buffer.h"
#include "../../Utility/includes/Token.h"
#include "../../Symboltable/includes/Symboltable.h"


#ifndef SCANNER_H_
#define SCANNER_H_

#include "IScanner.h"
#include <iostream>
using namespace std;

class Scanner {
public:
	virtual ~Scanner();
	static Scanner * create();

	virtual void update(Buffer * buffer, Symboltable * symtab) = 0;
	virtual Token nextToken() = 0;
	virtual void freeToken(Token token) = 0;
};

#endif /* SCANNER_H_ */
