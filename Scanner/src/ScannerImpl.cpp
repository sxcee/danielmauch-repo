#include "../../Buffer/includes/Buffer.h"
#include "../../Utility/includes/Token.h"
#include "../../Utility/includes/Information.h"
#include "../includes/ScannerImpl.h"
#include "../../Symboltable/includes/Symboltable.h"
#include "../../Automat/includes/Automat.h"
#include <iostream>

#define NULL __null

#include <iostream>
using namespace std;

Scanner * Scanner::create() {
	return (new ScannerImpl());
}

ScannerImpl::ScannerImpl(){
	tooMuchChars = 0;
	makeToken = false;
	row = 1;
	column = 1;
	tempType = TokenType::UNDEFINED;
	lexem = 0;
	currentLength = 0;
	lastChar = 0;
	cout << "ScannerImpl" << endl;
}

void ScannerImpl::update(Buffer * buffer, Symboltable * symtab) {
	this->buffer = buffer;
	this->symtab = symtab;
	this->automat = new Automat(this);
}

ScannerImpl::~ScannerImpl() {

}

Scanner::~Scanner(){}

Token ScannerImpl::nextToken() {
	makeToken = false;
	int currentRow = this->row;
	int currentColumn = this->column;
	Information * tempInfo;

	while(!makeToken){
		char tempChar = buffer->getChar();

		if(tempChar == '\n') {
			this->column++;
			this->row = 1;
			lastChar = '\n';
		}
		if(tempChar == ' ') {
			this->row++;
			lastChar = ' ';
		}
		if(tempChar == '\0') {
			lastChar = '\0';
		}

		automat->read(tempChar);

		if(lastChar != '\n' && lastChar != '\0' && lastChar != ' ') {
			//buchstaben an Wort anhängen
		}
		if(this->tooMuchChars != 0) {
			//aus aktuellem lexem tooMuchChars-viele Buchstaben entfernen
		}
	}
	if(symtab->getEntry(lexem) == NULL) {
		symtab->addEntry(lexem, tempType, 0);		//initialvalue lexem ?!?
		tempInfo = symtab->getEntry(lexem);
	} else {
		tempInfo = symtab->getEntry(lexem);
	}
	Token * returnToken = new Token(tempType, tempInfo, currentRow, currentColumn);
	//Token returnToken = new Token(tempType, Information* information, int row, int column);

	cout << "scanner geht" << endl;
	makeToken = false;

	return *returnToken;
}

void ScannerImpl::freeToken(Token token) {
	//removefunktion aus Symboltabelle ?!
}

void ScannerImpl::mkToken(TokenType type) {
	this->tempType = type;
	makeToken = true;
}

void ScannerImpl::ungetChar(int amount) {
	this->tooMuchChars = amount;
	this->buffer->ungetChar(amount);
}

void ScannerImpl::stop() {

}
