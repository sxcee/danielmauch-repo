#include "../includes/Buffer.h"
#include "../includes/Scanner.h"
#include "../includes/IScanner.h"
#include "../../Automat/includes/Automat.h"
#include "../includes/Scanner.h"
#include "../includes/ScannerImpl.h"
int main(int argc, char **argv) {
	Scanner * scanner;
	Buffer* buffer = new Buffer();
	Symboltable * symtab = new Symboltable();

	//create instance of scannerimpl
	scanner = Scanner::create();
	//link buffer and symtab to scanner
	scanner->update(buffer, symtab);

	scanner->nextToken();

}

