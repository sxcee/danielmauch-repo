/*
 * CommentState.cpp
 *
 *  Created on: Apr 26, 2015
 *      Author: tom
 */

#include "../includes/CommentState.h"

void CommentState::read(char c, AutomatOO* automat){
	if(c == '*'){
		flag = true;
	}

	if((c == ':') && flag){
		automat->mkToken(COMMENT);
		automat->setStateInitial();
	} else if( c != '*'){
		flag = false;
	}
}


