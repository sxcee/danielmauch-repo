/*
 * Automat.cpp
 *
 */

#include "../includes/Automat.h"
#include "../includes/State.h"
#include "../includes/InitialState.h"
#include "../includes/NumberState.h"
#include "../includes/IdentifierState.h"

#include <iostream>



Automat::Automat(IScanner* scanner) {
	_initialState = new InitialState();
	_numberState = new NumberState();
	_identifierState = new IdentifierState();
	_asignState = new AsignState();
	_commentState = new CommentState();
	_specialState = new SpecialState();
	_specialState2 = new SpecialState2();

	this->scanner = scanner;

	_currentState = _initialState;

}

Automat::~Automat() {
	delete _initialState;
	delete _numberState;
	delete _identifierState;
	delete _asignState;
	delete _commentState;
	delete _specialState;
	delete _specialState2;
	delete _currentState;
}



void Automat::read(char c){
	_currentState->read(c, this);
}

void Automat::setStateInitial(){
	this->_currentState = this->_initialState;
}

void Automat::setStateNumber(){
	this->_currentState = this->_numberState;
}

void Automat::setStateIdentifier(){
	this->_currentState = this->_identifierState;
}

void Automat::setStateAsign(){
	this->_currentState = this->_asignState;
}

void Automat::setStateComment(){
	this->_currentState = this->_commentState;
}

void Automat::setStateSpecial(){
	this->_currentState = this->_specialState;
}

void Automat::setStateSpecial2(){
	this->_currentState = this->_specialState2;
}

void Automat::mkToken(TokenType token){
	this->scanner->mkToken(token);

}


void Automat::ungetChar(int i){
	this->scanner->ungetChar(i);
}


