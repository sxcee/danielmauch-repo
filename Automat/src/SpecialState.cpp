/*
 * SpecialState.cpp
 *
 *  Created on: Apr 26, 2015
 *      Author: tom
 */

#include "../includes/SpecialState.h"

void SpecialState::read(char c, AutomatOO* automat){
	if(c == ':'){
		automat->setStateSpecial2();
	} else {
		automat->mkToken(LESS);
		automat->setStateInitial();
		automat->ungetChar(1);
	}
}


void SpecialState2::read(char c, AutomatOO* automat){
	if(c == '>'){
		automat->mkToken(SPECIAL);
		automat->setStateInitial();
	} else {
		automat->mkToken(LESS);
		automat->setStateInitial();
		automat->ungetChar(2);
	}
}

