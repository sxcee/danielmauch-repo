#ifndef INFORMATION_H_
#include "TokenType.h"

#define INFORMATION_H_

class Information {
public:
	Information(const char * lexem, TokenType type, int value);
	~Information();

	int getValue();
	TokenType getTokenType();
	const char * getLexem();
private:
	const char * lexem;
	TokenType type;
	int value;
};

#endif
