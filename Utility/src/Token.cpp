/*
 * Token.cpp
 *
 *  Created on: 16.04.2015
 *      Author: danielmauch
 */

#include "../includes/Token.h"

Token::Token(TokenType type, Information* information, int row, int column) {
	this->type = type;
	this->information = information;
	this->row = row;
	this->column = column;
}

Token::~Token() {

}

TokenType Token::getType() {
	return Token::type;
}

void Token::setType(TokenType type) {
	this->type = type;
}
