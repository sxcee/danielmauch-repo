#include "../includes/SymTabEntry.h"
#define NULL __null

SymTabEntry::SymTabEntry(const char * lexem, Information * information) {
	this->lexem = lexem;
	this->linkToInformation = information;
	this->next = NULL;
}

SymTabEntry::SymTabEntry() {
	this->lexem = NULL;
	this->linkToInformation = NULL;
	this->next = NULL;
}

SymTabEntry::~SymTabEntry() {

}

void SymTabEntry::setNextEntry(SymTabEntry * nextEntry) {
	this->next = nextEntry;
}

const char * SymTabEntry::getLexem() {
	return this->lexem;
}

Information * SymTabEntry::getInformation() {
	return this->linkToInformation;
}

SymTabEntry * SymTabEntry::getNextEntry() {
	return this->next;
}
